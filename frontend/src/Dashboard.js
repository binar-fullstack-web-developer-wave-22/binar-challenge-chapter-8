import { useState } from 'react';
import { Link } from 'react-router-dom';
import './Dashboard.css';

export default function Dashboard() {
  // const [filter, setFilter] = useState({
  //   username: '',
  //   email: '',
  //   experience: '',
  //   lvl: '',
  // });

  // let idData = 1;

  // const [player, setPlayer] = useState([]);

  // const onChange = (e) => {
  //   setFilter({ ...filter, ...{ [e.target.name]: e.target.value } });
  // };

  // const fetchPlayer = async () => {
  //   const response = await fetch('http://localhost:4000/api/v1/players')
  //     .then((response) => {
  //       setFilter(response.data.data);
  //       setPlayer(response.data.data);
  //     })
  //     .catch((err) => {
  //       console.log(err);
  //     });
  // };

  // const theFilter = () => {
  //   const resFilter = fetch('http://localhost:4000/api/v1/players', { method: 'GET' }, { params: filter })
  //     .then((resFilter) => {
  //       setPlayer(resFilter.data.data);
  //     })
  //     .catch((err) => {
  //       console.log(err);
  //     });
  //  onChange={(e) => setFilter(e.target.value)} };

  let examplePlayer = [
    {
      username: 'data1',
      email: 'd1@mail.com',
      experience: '1000',
      lvl: '1',
    },
    {
      username: 'data2',
      email: 'd2@mail.com',
      experience: '2000',
      lvl: '2',
    },
    {
      username: 'data3',
      email: 'd3@mail.com',
      experience: '3000',
      lvl: '3',
    },
  ];

  let idData = 1;

  const [player, newPlayer] = useState(examplePlayer);
  const showPlayer = (Player) => {
    newPlayer((player) => [...player, Player]);
  };

  return (
    <div className="container">
      <h1>Player Dashboard</h1>
      <div className="row mt-4">
        <div className="col form-group">
          <label htmlFor="username">Username</label>
          <input type="text" className="form-control" id="username" placeholder="username" />
        </div>
        <div className="col form-group">
          <label htmlFor="email" required>
            Email
          </label>
          <input type="email" className="form-control" id="email" placeholder="email" />
        </div>
        <div className="col form-group">
          <label htmlFor="experience">Expereience</label>
          <input type="number" className="form-control" id="experience" placeholder="0" />
        </div>
        <div className="col form-group">
          <label htmlFor="level">Level</label>
          <input type="number" className="form-control" id="level" placeholder="0" />
        </div>
        <div className="col mt-4">
          <button type="button" className="btn">
            Show
          </button>
        </div>
      </div>

      <div className="row mt-5">
        <div className="col">
          <Link to="/newPlayer">
            <button type="button" class="btn2">
              Create Player
            </button>
          </Link>
        </div>
      </div>

      <div className="row mt-4">
        <table class="table table-striped">
          <thead>
            <tr>
              <th>#</th>
              <th>Username</th>
              <th>Email</th>
              <th>Experience</th>
              <th>Level</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            {player.map((players) => {
              const { id, username, email, experience, lvl } = players;
              return (
                <tr key={id}>
                  <th scope="row">{idData++}</th>
                  <th>{players.username}</th>
                  <th>{players.email}</th>
                  <th>{players.experience}</th>
                  <th>{players.lvl}</th>
                  <th>
                    <Link to="/editPlayer">
                      <a href="/">Edit</a>
                    </Link>
                    <a href="/" className="ms-4 text-danger">
                      Delete
                    </a>
                  </th>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    </div>
  );
}
