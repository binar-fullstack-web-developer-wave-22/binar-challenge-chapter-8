import { BrowserRouter as Router, Routes, Route, Link } from 'react-router-dom';
import Dashboard from './Dashboard';
import './App.css';
import EditPlayer from './EditPlayer';
import NewPlayer from './NewPlayer';

function App() {
  return (
    <Router>
      <Routes>
        <Route path="/" element={<Dashboard />} />
        <Route path="/editPlayer" element={<EditPlayer />} />
        <Route path="/newPlayer" element={<NewPlayer />} />
      </Routes>
    </Router>
  );
}

export default App;
