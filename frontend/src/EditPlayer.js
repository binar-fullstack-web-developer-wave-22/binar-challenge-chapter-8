import './Dashboard.css';

export default function EditPlayer() {
  return (
    <div className="container">
      <h1>Edit Player</h1>
      <div className="row mt-4">
        <div className="col form-group">
          <label htmlFor="username">Username</label>
          <input type="text" className="form-control" id="username" placeholder="username" />
        </div>
        <div className="col form-group">
          <label htmlFor="email">Email</label>
          <input type="text" className="form-control" id="email" placeholder="name@mail.com" />
        </div>
        <div className="col form-group">
          <label htmlFor="password">Password</label>
          <input type="password" className="form-control" id="password" placeholder="password" />
        </div>
        <div className="col form-group">
          <label htmlFor="experience">Expereience</label>
          <input type="number" className="form-control" id="experience" placeholder="0" />
        </div>
        <div className="col form-group">
          <label htmlFor="level">Level</label>
          <input type="number" className="form-control" id="level" placeholder="0" />
        </div>
        <div className="col mt-4">
          <button type="button" className="btn">
            Save
          </button>
        </div>
      </div>
    </div>
  );
}
