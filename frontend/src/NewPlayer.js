import { useState } from 'react';
import { Link } from 'react-router-dom';

export default function NewPlayer({ createPlayer }) {
  const [username, setUsername] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPass] = useState('');
  const [experience, setXp] = useState(0);

  const submit = (e) => {
    let data = {
      username: username,
      email: email,
      password: password,
      experience: experience,
    };
    createPlayer(data);
  };

  return (
    <div className="container">
      <h1>Create New Player</h1>
      <form onSubmit={(e) => submit(e)}>
        <div className="row d-flex flex-column mt-3 gap-3">
          <div className="col">
            <label htmlFor="username">Username</label>
            <input type="text" onChange={(e) => setUsername(e.target.value)} className="form-control" id="username" placeholder="username" />
          </div>
          <div className="col">
            <label htmlFor="email">Email</label>
            <input type="email" onChange={(e) => setEmail(e.target.value)} className="form-control" id="email" placeholder="name@mail.com" />
          </div>
          <div className="col">
            <label htmlFor="password">Password</label>
            <input type="password" onChange={(e) => setPass(e.target.value)} className="form-control" id="password" placeholder="password" />
          </div>
          <div className="col">
            <label htmlFor="experience">Experience</label>
            <input type="text" onChange={(e) => setXp(e.target.value)} className="form-control" id="experience" placeholder="0" />
          </div>
          <div className="col mt-4 d-flex justify-content-end">
            <Link to="/">
              <button type="button" className="btn">
                Back
              </button>
            </Link>
            <button type="submit" className="btn">
              Submit
            </button>
          </div>
        </div>
      </form>
    </div>
  );
}
